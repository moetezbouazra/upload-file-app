const db = require("../models");

const multer = require('multer')
const path = require('path')

const Document = db.documents


// 1. create document
const addDocument = async (req, res) => {

    let info = {
        document: req.file.path,
        name: req.body.name,
        description: req.body.description,
        path: req.file.path,
        type: req.file.mimetype.split("/")[1]
    }

    console.log("req.file is////////////////////////////",req.file)
    const document = await Document.create(info)
    res.status(200).send(document)
    console.log(document)

}
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'Documents')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname))
    }
})

const upload = multer({
    storage: storage,
    limits: { fileSize: '1000000' },
}).single('document')

// get pages number
const getPagesNumber = async (req, res) => {
    const pageSize = req.query.pageSize || 10;

    let document = await Document.findAll({})
    let pageCount = Math.ceil(document.length / pageSize)
    res.status(200).json(pageCount).end()

}



// 2. get all documents

const getAllDocuments = async (req, res) => {
    const page = req.query.page || 1;
    const pageSize = req.query.pageSize || 10;


    let document = await Document.findAll({
        offset: (page - 1) * pageSize,
        limit: pageSize
    })
    res.status(200).send(document)

}

// 3. get single document by id

const getOneDocument = async (req, res) => {

    let id = req.params.id
    let document = await Document.findOne({ where: { id: id }})
    res.status(200).send(document)

}

// 4. update document

const updateDocument = async (req, res) => {

    let id = req.params.id

    const document = await Document.update(req.body, { where: { id: id }})

    res.status(200).send(document)
    console.log(req.body)
   

}

// 5. delete document by id

const deleteDocument = async (req, res) => {

    let id = req.params.id
    
    await Document.destroy({ where: { id: id }} )

    res.status(200).send('Document is deleted !')

}


module.exports = {
    addDocument,
    getAllDocuments,
    getOneDocument,
    updateDocument,
    deleteDocument,
    upload,
    getPagesNumber,
    
}