const express = require('express')
const documentRouter = require('./routes/documentRoutes.js')
var cors = require('cors')
require('dotenv').config()


const app = express()
app.use(cors())

const PORT = process.env.PORT
const docPath = process.env.DOCUMENT_STORAGE_URI


app.use(express.json())
app.use(express.urlencoded({ extended: true }))



app.use('/api/documents', documentRouter)

app.use('/Documents', express.static(docPath))



app.listen(PORT, () => {
    console.log("server is running on port 3005")
})