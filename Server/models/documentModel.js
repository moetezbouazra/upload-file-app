module.exports = (sequelize, DataTypes) => {

  const Document = sequelize.define("document", {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey:true
      },
      name: {
        type: DataTypes.CHAR(48)
      },
      type: {
        type: DataTypes.ENUM('TXT', 'DOCX','PDF')
      },
      description: {
        type: DataTypes.STRING
      },
      path: {
        type: DataTypes.STRING
      }
      
  },{
    paranoid: true
  }
  )

  return Document
}
