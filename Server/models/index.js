
const {Sequelize, DataTypes} = require('sequelize');

const sequelize = new Sequelize('test-db', 'username', 'password', {
    host: './dev.sqlite',
    dialect: 'sqlite'
  });

  sequelize.authenticate()
  .then(() => {
      console.log('connected..')
  })
  .catch(err => {
      console.log('Error'+ err)
  })


sequelize.sync({ force: false })
.then(() => {
    console.log('db is ready!')
})

var db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.documents = require('./documentModel.js')(sequelize, DataTypes)

module.exports = db;

