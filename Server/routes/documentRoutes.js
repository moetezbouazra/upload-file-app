
const documentController = require('../controllers/documentController.js')


const router = require('express').Router()


router.post('/addDocument', documentController.upload , documentController.addDocument)

router.get('/', documentController.getAllDocuments)

router.get('/count', documentController.getPagesNumber)

router.get('/:id', documentController.getOneDocument)

router.put('/:id', documentController.updateDocument)

router.delete('/:id', documentController.deleteDocument)

module.exports = router