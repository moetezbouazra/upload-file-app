import "./App.css";
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Upload from "./pages/Upload";
import View from "./pages/View";
import {Container, Nav, Navbar} from 'react-bootstrap';
import Edit from "./pages/Edit";
function App() {

  return (
    <Router>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/upload">Upload</Nav.Link>
            <Nav.Link href="/view">Documents</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Routes>
        <Route path="/" element={< Home/>} />
        <Route path="/upload" element={< Upload />} />
        <Route path="/view" element={< View />} />
        <Route path="/edit/:id" element={< Edit />} />
      </Routes>
    </Router>
  );
}

export default App;
