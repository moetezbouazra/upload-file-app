import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Container, Table, Button, Pagination, Form, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';


const View = () => {
  const [documents, setDocuments] = useState([])
  const [del, setDel] = useState(false)
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [pageCount, setPageCount] = useState(1);

  function handleChange(event) {
    setPageSize(event.target.value);
    setPage(1)
  }

  const handleDelete = async (id) => {
    await axios.delete(`http://localhost:3005/api/documents/${id}`)
    console.log("Deleted successfully")
    setDel(true)
  }

  useEffect(() => {
    const getDocumentsData = async () => {
      const { data } = await axios.get(`http://localhost:3005/api/documents/?page=${page}&pageSize=${pageSize}`)
      setDocuments(data)
      setDel(false)


    }
    getDocumentsData()
  }, [del, page, pageSize])

  useEffect(() => {
    const getPageCount = async () => {
      const { data } = await axios.get(`http://localhost:3005/api/documents/count/?pageSize=${pageSize}`)
      setPageCount(data)
      setDel(false)


    }
    getPageCount()
  }, [pageSize])

  const handleDownload = async (doc) => {
    switch (doc.type) {
      case "pdf":
        doc.type = 'pdf'
        break;
      case "vnd.openxmlformats-officedocument.wordprocessingml.document":
        doc.type = 'docx'
        break;
      case "plain":
        doc.type = 'txt'
        break;
      default:
        console.log("invalid document type")
    }
    try {
      const response = await axios.get(`http://localhost:3005\\${doc.path}`);
      const file = new Blob([response.data], { type: doc.type });
      const fileUrl = URL.createObjectURL(file);
      const link = document.createElement("a");
      link.href = fileUrl;
      link.download = `${doc.name}.${file.type}`;
      document.body.appendChild(link);
      link.click();
      link.remove();
    } catch (err) {
      console.error(err);
    }
  }

  const typeHandler = (x) => {
    switch (x) {
      case "pdf":
        return "PDF";
      case "vnd.openxmlformats-officedocument.wordprocessingml.document":
        return "Word";
      case "plain":
        return "TXT";
      default:
        return "Other";
    }
  }

  return (
    <>
      <Container className="justify-content-center p-2">
        <h1 className='text-center'>Uploaded Documents</h1>
        <hr />

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {
              Array.isArray(documents) && documents.map((document, key) =>
                <tr key={document.id}>
                  <td>{page > 1 ? ((page - 1) * pageSize) + key + 1 : key + 1}</td>
                  <td>{document.name}</td>
                  <td>{document.description}</td>
                  <td>{typeHandler(document.type)}</td>
                  <td>
                    <Row> <Col sm={2}>
                      <Link to={`/edit/${document.id}`}>
                        <Button variant="outline-warning"  ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16"> <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" /> </svg></Button> </Link>
                    </Col>
                      <Col md={2}>
                        <Button variant="outline-success" onClick={() => handleDownload(document)}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-cloud-download" viewBox="0 0 16 16"> <path d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z" /> <path d="M7.646 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V5.5a.5.5 0 0 0-1 0v8.793l-2.146-2.147a.5.5 0 0 0-.708.708l3 3z" /> </svg></Button>
                      </Col>
                      <Col sm={2}>
                        <Button variant="outline-danger" onClick={() => handleDelete(document.id)}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16"> <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" /> <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" /> </svg></Button>
                      </Col>

                    </Row>
                  </td>
                </tr>)
            }
          </tbody>
        </Table>



        <Row className="justify-content-end">
          <Col md={10}>
            <Pagination className="justify-content-center p-2">
              <Pagination.First onClick={() => setPage(1)} />
              <Pagination.Prev onClick={() => page > 1 ? setPage(page - 1) : null} />
              <Pagination.Item onClick={() => setPage(1)} hidden={page === 1}>{1}</Pagination.Item>

              <Pagination.Ellipsis hidden={page < 3} />
              <Pagination.Item active>{page}</Pagination.Item>
              <Pagination.Ellipsis hidden={page > (pageCount - 2)} />
              <Pagination.Item onClick={() => setPage(pageCount)} hidden={page >= pageCount}>{pageCount}</Pagination.Item>
              <Pagination.Next onClick={() => page < pageCount ? setPage(page + 1) : null} />
              <Pagination.Last onClick={() => setPage(pageCount)} />
            </Pagination>
          </Col>

          <Col md={2}>
            <Form.Select size="md" style={{ width: 80 }} onChange={handleChange} value={pageSize}>
              <option value="5" >{5}</option>
              <option value="10"> {10} </option>
              <option value="20"> {20} </option>
            </Form.Select>
          </Col >
        </Row>
      </Container>
    </>
  )
}

export default View