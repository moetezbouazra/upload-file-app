import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Container, Form, Button } from 'react-bootstrap';
import {  useParams } from "react-router-dom";


const Edit = () => {

    let data
    let params = useParams();
    let id =params.id

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [document, setDocument] = useState('')


    const defaultValues = async () => {

        data = await axios.get(`http://localhost:3005/api/documents/${id}`)
        setName(data.data.name)
        setDescription(data.data.description)
    }

    useEffect(() => {
    
      return () => {
        defaultValues()
      }
    }, [])
    
    


    const submitChangesHandler = async (e) => {

        e.preventDefault()

        const data = {
            name: name,
            description: description,
            document: document
        }

        await axios.put(`http://localhost:3005/api/documents/${id}`, data)
        console.log("Document edited successsfully")

    }
    return (
        <div>
            <Container className='mt-5 p-2'>
                <h1>Edit Document</h1>
                <hr />

                <Form onSubmit={submitChangesHandler} method="POST" encType='multipart/form-data'>

                <Form.Group controlId="file" className="mb-3">
                    <Form.Label>Upload Document</Form.Label>
                    <Form.Control
                        type="file"
                        name='document'
                        onChange={(e) => setDocument(e.target.files[0])}
                        size="lg" />
                </Form.Group>

                    <Form.Group className="mb-3" controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            type="text"
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            as="textarea"
                        />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Apply changes
                    </Button>
                </Form>
            </Container>
        </div>
    )
}

export default Edit