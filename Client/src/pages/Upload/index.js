import React, { useState } from 'react';
import axios from 'axios';
import { Container, Form, Button, Alert } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';



const Upload = () => {

  const [isLoading, setLoading] = useState(false);
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [document, setDocument] = useState('')
  const [validated, setValidated] = useState({
    name: true,
    file: true
  });


  let navigate = useNavigate()
  const apiUrl = process.env.REACT_APP_API_URL



  const addDocumentHandler = async (e) => {
    setLoading(true)
    e.preventDefault()

    //Input validation 
let x = {
  ...validated
}
    if (!name) {
      x = {
        ...x,
        name: false
      }
      setValidated(v => ({
        ...v,
        name: false
      }))
    }else{
      x = {
        ...x,
        name: true
      }
      setValidated(v => ({
        ...v,
        name: true
      }))
    }


    if (!document ||  !(document.type === 'application/pdf' ||  document.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || document.type === 'text/plain' ) ) {
      console.log("document.type",document.type)
      x = {
        ...x,
        file: false
      }
      setValidated(v => ({
        ...v,
        file: false
      }))
    }else{
      x = {
        ...x,
        file: true
      }
      setValidated(v => ({
        ...v,
        file: true
      }))
    }


    if (!(x.name && x.file)) {
      console.log("fields are missing !")
      setLoading(false)
      return;
    }
    setValidated(v => ({
      ...v,
      name: true,
      file:true
    }))
      const formData = new FormData();
      formData.append('document', document)
      formData.append('name', name)
      formData.append('description', description)

      try {
        const response = await axios({
          method: "post",
          url: `${apiUrl}api/documents/addDocument`,
          data: formData,
          headers: { "Content-Type": "multipart/form-data" },
        });
        setLoading(false)
        navigate('/View')

      } catch (error) {
        console.log(error)
      }
    

  }
 


  return (
    <>
      <Container className='mt-5 p-2'>
        <h1>Add Document</h1>
        <hr />

        <Form onSubmit={addDocumentHandler} method="POST" encType='multipart/form-data'>

          <Form.Group controlId="document" className="mb-3" validated={validated.file ? 'success' : 'error'}>
            <Form.Label>Upload document</Form.Label>
            <Form.Control
              type="file"
              name='document'
              onChange={(e) => setDocument(e.target.files[0])}
              size="lg"
            />
            <Form.Control.Feedback />
            {!validated.file && <Alert variant='danger'>File missing or insupported file format (supported formats are: .doc, pdf and .txt)</Alert>}
          </Form.Group>

          <Form.Group className="mb-3" controlId="name"
            validated={validated.name ? 'success' : 'error'}>
            <Form.Label>Name</Form.Label>
            <Form.Control
              value={name}
              name='name'
              placeholder="Enter a name"
              onChange={(e) => setName(e.target.value)}
              type="text"
            />
            <Form.Control.Feedback />
            {!validated.name && <Alert variant='danger'>Please insert a name</Alert>}
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control
              value={description}
              placeholder="Enter a description"
              onChange={(e) => setDescription(e.target.value)}
              as="textarea"
            />

          </Form.Group>

          <Button variant="primary" type="submit" disabled={isLoading}>
            {isLoading ? 'Uploading…' : 'Upload Document'}
          </Button>
        </Form>
      </Container>

    </>
  )
}

export default Upload;